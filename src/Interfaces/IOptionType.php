<?php  namespace Aedart\ArrayOption\Interfaces; 

/**
 * Interface Option Type
 *
 * Set of allowed / supported data types for array-options. These correspond
 * to the native data types of PHP
 *
 * @see gettype()
 *
 * @author Alin Eugen Deac <aedart@gmail.com>
 * @package Aedart\ArrayOption\Interfaces
 */
interface IOptionType {

    /**
     * Data type string
     */
    const TYPE_STRING = 'string';

    /**
     * Data type integer
     */
    const TYPE_INTEGER = 'integer';

    /**
     * Data type float
     */
    const TYPE_FLOAT = 'double'; // @see PHP gettype() method

    /**
     * Data type boolean
     */
    const TYPE_BOOLEAN = 'boolean';

    /**
     * Data type array
     */
    const TYPE_ARRAY = 'array';

    /**
     * Data type object
     */
    const TYPE_OBJECT = 'object';

    /**
     * Data type null
     */
    const TYPE_NULL = 'null';

    /**
     * Data type resource
     */
    const TYPE_RESOURCE = 'resource';

}