<?php  namespace Aedart\ArrayOption\Exception; 

/**
 * Class Invalid Option-Required (Flag) Exception
 *
 * Throw this exception if option require-flag/state is invalid, e.g. not a boolean or if
 * given value was unexpected
 *
 * @author Alin Eugen Deac <aedart@gmail.com>
 * @package Aedart\ArrayOption\Exception
 */
class InvalidOptionRequiredFlagException extends \InvalidArgumentException{

}