<?php  namespace Aedart\ArrayOption\Interfaces; 

/**
 * Interface Array Option Aware
 *
 * Components that implement this, are able to set and retrieve an
 * array-options-list
 *
 * @see IArrayOptionsList
 *
 * @author Alin Eugen Deac <aedart@gmail.com>
 * @package Aedart\ArrayOption\Interfaces
 */
interface ArrayOptionsListAware {

    /**
     * Set this component's options list
     *
     * @param IArrayOptionsList $list List of options
     *
     * @return void
     */
    public function setArrayOptionsList(IArrayOptionsList $list);

    /**
     * Get this component's options list
     *
     * If no options list has been set, this method obtains, sets and
     * returns a default options list
     *
     * @see getDefaultArrayOptionsList()
     *
     * @return IAllowedOptionsList List of options
     */
    public function getArrayOptionsList();

    /**
     * Get a default options list instance
     *
     * @return IArrayOptionsList A default instance of a "options-list"
     */
    public function getDefaultArrayOptionsList();

}