<?php

use Aedart\ArrayOption\Traits\ValueTypeValidateTrait;
use Aedart\ArrayOption\Interfaces\IOptionType;

/**
 * Class ValueTypeValidateTraitTest
 *
 * @coversDefaultClass Aedart\ArrayOption\Traits\ValueTypeValidateTrait
 *
 * @author Alin Eugen Deac <aedart@gmail.com>
 */
class ValueTypeValidateTraitTest extends \Codeception\TestCase\Test
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    /******************************************************************************
     * Providers
     *****************************************************************************/

    /**
     * Get a mock for the trait in question
     *
     * @return Aedart\ArrayOption\Traits\ValueTypeValidateTrait
     */
    protected function getTraitMock(){
        $m = $this->getMockForTrait('Aedart\ArrayOption\Traits\ValueTypeValidateTrait');
        return $m;
    }

    /**
     * Data provider for tests
     *
     * @return array
     */
    public function dataProvider(){
        return [
            [1, IOptionType::TYPE_INTEGER, true],
            [42.4, [IOptionType::TYPE_INTEGER, IOptionType::TYPE_FLOAT], true],
            ["bla", [IOptionType::TYPE_STRING, IOptionType::TYPE_NULL], true],
            ["no way", IOptionType::TYPE_ARRAY, false],
            [true, IOptionType::TYPE_OBJECT, false],
            [[1, 2, 3], IOptionType::TYPE_ARRAY, true],
        ];
    }

    /******************************************************************************
     * Tests
     *****************************************************************************/

    /**
     * @test
     * @covers ::isValueTypeAllowed
     * @covers ::isTypeInList
     *
     * @dataProvider dataProvider
     *
     * @param mixed $value
     * @param string|string[] $allowed
     * @param bool $expectedResult
     */
    public function valueTypeValid($value, $allowed, $expectedResult){
        $trait = $this->getTraitMock();
        $this->assertSame($expectedResult, $trait->isValueTypeAllowed($value, $allowed));
    }

    /**
     * @test
     * @covers ::isValueTypeAllowed
     *
     * @expectedException \Aedart\ArrayOption\Exception\InvalidOptionTypeException
     */
    public function invalidAllowedList(){
        $trait = $this->getTraitMock();
        $trait->isValueTypeAllowed('myVar', false);
    }
}