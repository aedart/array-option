<?php

use Aedart\ArrayOption\Traits\OptionTypeValidateTrait;
use Aedart\ArrayOption\Interfaces\IOptionType;

/**
 * Class OptionTypeValidateTraitTest
 *
 * @coversDefaultClass Aedart\ArrayOption\Traits\OptionTypeValidateTrait
 *
 * @author Alin Eugen Deac <aedart@gmail.com>
 */
class OptionTypeValidateTraitTest extends \Codeception\TestCase\Test
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    /******************************************************************************
     * Providers
     *****************************************************************************/

    /**
     * Get a mock for the trait in question
     *
     * @return Aedart\ArrayOption\Traits\OptionTypeValidateTrait
     */
    protected function getTraitMock(){
        $m = $this->getMockForTrait('Aedart\ArrayOption\Traits\OptionTypeValidateTrait');
        return $m;
    }

    /**
     * Data provider
     *
     * @return array
     */
    public function typesDataProvider(){
        return [
            [IOptionType::TYPE_STRING, true],
            [IOptionType::TYPE_RESOURCE, true],
            [IOptionType::TYPE_OBJECT, true],
            [IOptionType::TYPE_NULL, true],
            [IOptionType::TYPE_ARRAY, true],
            [IOptionType::TYPE_BOOLEAN, true],
            [IOptionType::TYPE_FLOAT, true],
            [IOptionType::TYPE_INTEGER, true],
            [[IOptionType::TYPE_INTEGER, IOptionType::TYPE_FLOAT, IOptionType::TYPE_NULL], true],
            ['some special type', false],
            [42, false],
            [false, false],
            [true, false],
            [['a', 22], false],
        ];
    }

    /******************************************************************************
     * Tests
     *****************************************************************************/

    /**
     * @test
     * @covers ::isOptionTypeValid
     * @covers ::isStrOptionTypeValid
     * @covers ::isArrOptionTypeValid
     *
     * @dataProvider typesDataProvider
     *
     * @param mixed $type
     * @param bool $expectedResult
     */
    public function validStrInputType($type, $expectedResult){
        $trait = $this->getTraitMock();
        $this->assertSame($expectedResult, $trait->isOptionTypeValid($type));
    }
}