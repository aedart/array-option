<?php  namespace Aedart\ArrayOption;

use Aedart\ArrayOption\Exception\InvalidOptionDescriptionException;
use Aedart\ArrayOption\Exception\InvalidOptionNameException;
use Aedart\ArrayOption\Exception\InvalidOptionRequiredFlagException;
use Aedart\ArrayOption\Exception\InvalidOptionTypeException;
use Aedart\ArrayOption\Exception\InvalidOptionValueException;
use Aedart\ArrayOption\Interfaces\IAllowedOption;
use Aedart\ArrayOption\Traits\OptionNameValidateTrait;
use Aedart\ArrayOption\Traits\OptionTypeValidateTrait;
use Aedart\ArrayOption\Traits\ValueTypeValidateTrait;
use Aedart\Overload\Traits\Helper\ReflectionTrait;
use Aedart\Overload\Traits\GetterInvokerTrait;
use Aedart\Overload\Traits\SetterInvokerTrait;
use Aedart\ArrayOption\Interfaces\IOptionType;

/**
 * Class Allowed-Option
 *
 * @see IAllowedOption
 *
 * @author Alin Eugen Deac <aedart@gmail.com>
 * @package Aedart\ArrayOption
 */
class AllowedOption implements IAllowedOption{

    use OptionNameValidateTrait,
        OptionTypeValidateTrait,
        ValueTypeValidateTrait,
        ReflectionTrait,
        GetterInvokerTrait,
        SetterInvokerTrait;

    /**
     * Name of this allowed option
     *
     * @var string
     */
    protected $name = '';

    /**
     * The allowed data-type(s)
     *
     * @var string|string[]
     */
    protected $type = '';

    /**
     * Require state/flag - True if this option is required, false if not
     *
     * @var bool
     */
    protected $required = false;

    /**
     * A default value to be used
     *
     * @var mixed
     */
    protected $defaultValue = null;

    /**
     * A short description of this allowed option
     *
     * @var string
     */
    protected $description = '';

    /**
     * Constructor
     *
     * @param array $data [Optional][Default []] Allowed-option properties
     *
     * @see populate()
     * @see setName()
     * @see setType()
     * @see setRequired()
     * @see setDefaultValue()
     * @see setDescription()
     */
    public function __construct(array $data = []){
        if(!empty($data)){
            $this->populate($data);
        }
    }

    public function populate(array $data) {
        foreach($data as $key => $value){
            $this->__set($key, $value);
        }
    }

    public function setName($name) {
        if(!$this->isOptionNameValid($name)){
            throw new InvalidOptionNameException(sprintf('Option name "%s" is invalid', $name));
        }
        $this->name = $name;
    }

    public function getName() {
        return $this->name;
    }

    public function setType($type = IOptionType::TYPE_NULL) {
        if(!$this->isOptionTypeValid($type)){
            throw new InvalidOptionTypeException(sprintf('Option type "%s" is not allowed / supported. See IOptionType for further information', $type));
        }
        $this->type = $type;
    }

    public function getType() {
        return $this->type;
    }

    public function setRequired($isRequired) {
        if(!is_bool($isRequired)){
            throw new InvalidOptionRequiredFlagException(sprintf('Option required state / flag "%s" must be a valid boolean value', var_export($isRequired, true)));
        }
        $this->required = $isRequired;
    }

    public function getRequired() {
        return $this->required;
    }

    public function isRequired() {
        return $this->getRequired();
    }

    public function setDefaultValue($value = null) {
        $allowedType = $this->getType();

        if(!$this->isValueTypeAllowed($value, $allowedType)){
            throw new InvalidOptionValueException(sprintf('Default value "%s" is of the type %s, but only (%s) type(s) is/are allowed', $value, gettype($value), print_r($allowedType, true)));
        }
        $this->defaultValue = $value;
    }

    public function getDefaultValue() {
        return $this->defaultValue;
    }

    public function setDescription($desc = '') {
        if(!is_string($desc) || strlen($desc) > 250){
            throw new InvalidOptionDescriptionException(sprintf('Description must be a string of no more than 250 characters; current description length: "%s"', strlen($desc)));
        }
        $this->description = $desc;
    }

    public function getDescription() {
        return $this->description;
    }

    public function toArray() {
        return [
            'name' => $this->getName(),
            'type' => $this->getType(),
            'required' => $this->isRequired(),
            'defaultValue'  => $this->getDefaultValue(),
            'description' => $this->getDescription()
        ];
    }
}