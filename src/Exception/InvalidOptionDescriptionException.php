<?php  namespace Aedart\ArrayOption\Exception; 

/**
 * Class Invalid Option-Description Exception
 *
 * Throw this exception if an invalid option description has been provided
 *
 * @author Alin Eugen Deac <aedart@gmail.com>
 * @package Aedart\ArrayOption\Exception
 */
class InvalidOptionDescriptionException extends \InvalidArgumentException{

}