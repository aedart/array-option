<?php  namespace Aedart\ArrayOption\Exception; 

/**
 * Class Required Option Exception
 *
 * Throw this exception if an option was required but not provided
 *
 * @author Alin Eugen Deac <aedart@gmail.com>
 * @package Aedart\ArrayOption\Exception
 */
class RequiredOptionException extends \InvalidArgumentException{

}