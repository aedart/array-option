<?php

use Aedart\ArrayOption\AllowedOption;
use Aedart\ArrayOption\Interfaces\IOptionType;

/**
 * Class AllowedOptionTest
 *
 * @coversDefaultClass Aedart\ArrayOption\AllowedOption
 *
 * @author Alin Eugen Deac <aedart@gmail.com>
 */
class AllowedOptionTest extends \Codeception\TestCase\Test
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    /******************************************************************************
     * Providers
     *****************************************************************************/

    /**
     * Get an array with valid properties
     *
     * @return array
     */
    protected function getValidProperties(){
        return [
            'name' => 'my_option',
            'type' => IOptionType::TYPE_FLOAT,
            'required' => true,
            'defaultValue'  => 42.1,
            'description' => 'This is my custom array option'
        ];
    }

    /******************************************************************************
     * Tests
     *****************************************************************************/

    /**
     * @test
     * @covers ::__construct
     */
    public function createInstanceWithoutData(){
        try {
            $allowedOption = new AllowedOption();
        }catch (\Exception $e){
            $this->fail(sprintf('Could not create new instance; %s', PHP_EOL . $e));
        }

        // Means that the instance could be created
        $this->assertTrue(true);
    }


    /**
     * @test
     *
     * @covers ::__construct
     * @covers ::populate
     * @covers ::setName
     * @covers ::getName
     * @covers ::setType
     * @covers ::getType
     * @covers ::setRequired
     * @covers ::getRequired
     * @covers ::isRequired
     * @covers ::setDefaultValue
     * @covers ::getDefaultValue
     * @covers ::setDescription
     * @covers ::getDescription
     */
    public function settersAndGetters(){
        $data = $this->getValidProperties();

        $allowedOption = new AllowedOption($data);

        $this->assertSame($data['name'], $allowedOption->getName(), 'Invalid name');
        $this->assertSame($data['type'], $allowedOption->getType(), 'Invalid type');
        $this->assertSame($data['required'], $allowedOption->isRequired(), 'Invalid required');
        $this->assertSame($data['defaultValue'], $allowedOption->getDefaultValue(), 'Invalid default value');
        $this->assertSame($data['description'], $allowedOption->getDescription(), 'Invalid description');
    }

    /**
     * @test
     *
     * @covers ::__construct
     * @covers ::setName
     *
     * @expectedException \Aedart\ArrayOption\Exception\InvalidOptionNameException
     */
    public function invalidName(){
        $data = $this->getValidProperties();
        $data['name'] = 'my invalid name';

        $allowedOption = new AllowedOption($data);
    }

    /**
     * @test
     *
     * @covers ::__construct
     * @covers ::setType
     *
     * @expectedException \Aedart\ArrayOption\Exception\InvalidOptionTypeException
     */
    public function invalidType(){
        $data = $this->getValidProperties();
        $data['type'] = 'my type';

        $allowedOption = new AllowedOption($data);
    }

    /**
     * @test
     *
     * @covers ::__construct
     * @covers ::setRequired
     *
     * @expectedException \Aedart\ArrayOption\Exception\InvalidOptionRequiredFlagException
     */
    public function invalidRequired(){
        $data = $this->getValidProperties();
        $data['required'] = [1, 2, 3];

        $allowedOption = new AllowedOption($data);
    }

    /**
     * @test
     *
     * @covers ::__construct
     * @covers ::setDefaultValue
     *
     * @expectedException \Aedart\ArrayOption\Exception\InvalidOptionValueException
     */
    public function invalidDefaultValue(){
        $data = $this->getValidProperties();
        $data['type'] = IOptionType::TYPE_INTEGER;
        $data['defaultValue'] = 13.5;

        $allowedOption = new AllowedOption($data);
    }

    /**
     * @test
     *
     * @covers ::__construct
     * @covers ::setDescription
     *
     * @expectedException \Aedart\ArrayOption\Exception\InvalidOptionDescriptionException
     */
    public function invalidDescriptionType(){
        $data = $this->getValidProperties();
        $data['description'] = null;

        $allowedOption = new AllowedOption($data);
    }

    /**
     * @test
     *
     * @covers ::__construct
     * @covers ::setDescription
     *
     * @expectedException \Aedart\ArrayOption\Exception\InvalidOptionDescriptionException
     */
    public function invalidDescriptionLength(){
        $data = $this->getValidProperties();
        $description = '';

        for($i = 0; $i < 255; $i++){
            $description .= 'a';
        }

        $data['description'] = $description;

        $allowedOption = new AllowedOption($data);
    }

    /**
     * @test
     * @covers ::toArray
     */
    public function toArray(){
        $properties = $this->getValidProperties();

        $allowedOption = new AllowedOption();
        $allowedOption->populate($properties);

        $this->assertSame($properties, $allowedOption->toArray());
    }
}