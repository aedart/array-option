<?php

use Aedart\ArrayOption\Interfaces\AllowedOptionsListAware;
use Aedart\ArrayOption\Traits\AllowedOptionsListTrait;
use \Mockery;

/**
 * Class AllowedOptionsListTraitTest
 *
 * @coversDefaultClass Aedart\ArrayOption\Traits\AllowedOptionsListTrait
 *
 * @author Alin Eugen Deac <aedart@gmail.com>
 */
class AllowedOptionsListTraitTest extends \Codeception\TestCase\Test
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
        Mockery::close();
    }

    /******************************************************************************
     * Providers
     *****************************************************************************/

    /**
     * Get a mock for the trait in question
     *
     * @return PHPUnit_Framework_MockObject_MockObject|Aedart\ArrayOption\Interfaces\AllowedOptionsListAware
     */
    protected function getTraitMock(){
        $m = $this->getMockForTrait('Aedart\ArrayOption\Traits\AllowedOptionsListTrait');
        return $m;
    }

    /**
     * Get allowed options list mock
     *
     * @return \Mockery\MockInterface|Aedart\ArrayOption\Interfaces\IAllowedOptionsList
     */
    protected function getAllowedOptionsListMock(){
        $m = Mockery::mock('Aedart\ArrayOption\AllowedOptionsList');
        return $m;
    }

    /******************************************************************************
     * Tests
     *****************************************************************************/

    /**
     * @test
     * @covers ::getAllowedOptionsList
     * @covers ::setAllowedOptionsList
     * @covers ::getDefaultAllowedOptionsList
     */
    public function getDefaultAllowedOptionsList(){
        $mock = $this->getTraitMock();
        $this->assertInstanceOf('Aedart\ArrayOption\Interfaces\IAllowedOptionsList', $mock->getAllowedOptionsList());
    }

    /**
     * @test
     * @covers ::getAllowedOptionsList
     * @covers ::setAllowedOptionsList
     */
    public function setAndGetAllowedOptionsList(){
        $mock = $this->getTraitMock();

        $allowedOptionsList = $this->getAllowedOptionsListMock();

        $mock->setAllowedOptionsList($allowedOptionsList);

        $this->assertSame($allowedOptionsList, $mock->getAllowedOptionsList());
    }
}