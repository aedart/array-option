<?php  namespace Aedart\ArrayOption\Traits; 

/**
 * Trait Option-Name Validate
 *
 * @author Alin Eugen Deac <aedart@gmail.com>
 * @package Aedart\ArrayOption\Traits
 */
trait OptionNameValidateTrait {

    /**
     * Check if the given option name appears valid or not
     *
     * @param mixed $optionName The option name to be validated
     *
     * @return bool True if the given option name is a valid string, false if not
     */
    public function isOptionNameValid($optionName){
        if(is_string($optionName) && preg_match("/^[a-zA-Z]+[-_a-zA-Z0-9]*$/", $optionName) === 1){
            return true;
        }
        return false;
    }

}