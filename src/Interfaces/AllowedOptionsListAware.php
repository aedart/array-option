<?php  namespace Aedart\ArrayOption\Interfaces; 

use Aedart\ArrayOption\Interfaces\IAllowedOptionsList;

/**
 * Interface Allowed Options List Aware
 *
 * Components that implement this interface, promise that a allowed-options-list can be
 * specified and obtained, whenever it is needed
 *
 * @author Alin Eugen Deac <aedart@gmail.com>
 * @package Aedart\ArrayOption\Interfaces
 */
interface AllowedOptionsListAware {

    /**
     * Set this component's allowed options list
     *
     * @param IAllowedOptionsList $list List of allowed options
     *
     * @return void
     */
    public function setAllowedOptionsList(IAllowedOptionsList $list);

    /**
     * Get this component's allowed options list
     *
     * If no options list has been set, this method obtains, sets and
     * returns a default allowed options list
     *
     * @see getDefaultAllowedOptionsList()
     *
     * @return IAllowedOptionsList List of allowed options
     */
    public function getAllowedOptionsList();

    /**
     * Get a default allowed options list instance
     *
     * @return IAllowedOptionsList A default instance of a "allowed-options-list"
     */
    public function getDefaultAllowedOptionsList();
}