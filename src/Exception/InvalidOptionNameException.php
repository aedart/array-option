<?php  namespace Aedart\ArrayOption\Exception; 

/**
 * Class Invalid Option-Name Exception
 *
 * Throw this exception whenever an invalid option name has been provided
 *
 * @author Alin Eugen Deac <aedart@gmail.com>
 * @package Aedart\ArrayOption\Exception
 */
class InvalidOptionNameException extends \InvalidArgumentException{

}