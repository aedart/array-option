<?php  namespace Aedart\ArrayOption\Traits;

use Aedart\ArrayOption\Interfaces\IAllowedOptionsList;
use Aedart\ArrayOption\AllowedOptionsList;

/**
 * Trait Allowed Options List
 *
 * @see AllowedOptionsListAware
 *
 * @author Alin Eugen Deac <aedart@gmail.com>
 * @package Aedart\ArrayOption\Traits
 */
trait AllowedOptionsListTrait {

    /**
     * List of allowed options
     *
     * @var IAllowedOptionsList|null
     */
    protected $allowedOptionsList = null;

    /**
     * Set this component's allowed options list
     *
     * @param IAllowedOptionsList $list List of allowed options
     *
     * @return void
     */
    public function setAllowedOptionsList(IAllowedOptionsList $list){
        $this->allowedOptionsList = $list;
    }

    /**
     * Get this component's allowed options list
     *
     * If no options list has been set, this method obtains, sets and
     * returns a default allowed options list
     *
     * @see getDefaultAllowedOptionsList()
     *
     * @return IAllowedOptionsList List of allowed options
     */
    public function getAllowedOptionsList(){
        if(is_null($this->allowedOptionsList)){
            $this->setAllowedOptionsList($this->getDefaultAllowedOptionsList());
        }
        return $this->allowedOptionsList;
    }

    /**
     * Get a default allowed options list instance
     *
     * @return IAllowedOptionsList A default instance of a "allowed-options-list"
     */
    public function getDefaultAllowedOptionsList(){
        return new AllowedOptionsList();
    }

}