<?php  namespace Aedart\ArrayOption\Traits;
use Aedart\ArrayOption\Exception\InvalidOptionTypeException;

/**
 * Trait Type Comparison
 *
 * Offers validate / comparison utility methods
 *
 * @author Alin Eugen Deac <aedart@gmail.com>
 * @package Aedart\ArrayOption\Traits
 */
trait ValueTypeValidateTrait {

    /**
     * Check if the given value has a valid data-type - one that is allowed / supported.
     *
     * @param mixed $value The value who's data-type must be validated
     * @param string|string[] $allowedType The allowed data-type(s)
     *
     * @return bool True if the given data-type of the value is among the allowed types
     *
     * @throws InvalidOptionTypeException If given allowed-type is not a string or array
     */
    public function isValueTypeAllowed($value, $allowedType){
        $type = strtolower(gettype($value));

        if(is_array($allowedType)){
            return $this->isTypeInList($type, $allowedType);
        } else if(is_string($allowedType)){
            return $this->isTypeInList($type, [$allowedType]);
        } else {
            $m = sprintf('Cannot validate if data-type of "%s" (%s) is allowed; given list ($allowedType) must either be a string or array, %s given', $value, $type, gettype($allowedType));
            throw new InvalidOptionTypeException($m);
        }
    }

    /**
     * Check if the given data-type (string representation) is found
     * in the given list of data-types (also string representation)
     *
     * @param string $type Data-type (string representation) of some value
     * @param array $list List of values
     *
     * @return bool True if the given type is amongst the values in the given list
     */
    protected function isTypeInList($type, array $list){
        foreach($list as $allowedType){
            if($type == strtolower($allowedType)){
                return true;
            }
        }
        return false;
    }
}