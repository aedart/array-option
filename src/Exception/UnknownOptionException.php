<?php  namespace Aedart\ArrayOption\Exception; 

/**
 * Class Unknown Option Exception
 *
 * Throw this exception if an unknown / unsupported option was provided
 *
 * @author Alin Eugen Deac <aedart@gmail.com>
 * @package Aedart\ArrayOption\Exception
 */
class UnknownOptionException extends \InvalidArgumentException{

}