<?php

use Aedart\ArrayOption\Interfaces\IAllowedOptionsAssert;
use Aedart\ArrayOption\Traits\AllowedOptionsAssertTrait;
use Aedart\ArrayOption\Interfaces\IOptionType;
use \Mockery;

/**
 * Class AllowedOptionsAssertTraitTest
 *
 * @coversDefaultClass Aedart\ArrayOption\Traits\AllowedOptionsAssertTrait
 *
 * @author Alin Eugen Deac <aedart@gmail.com>
 */
class AllowedOptionsAssertTraitTest extends \Codeception\TestCase\Test
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
        Mockery::close();
    }

    /******************************************************************************
     * Providers
     *****************************************************************************/

    /**
     * Get a mock for the trait in question
     *
     * @return PHPUnit_Framework_MockObject_MockObject|Aedart\ArrayOption\Interfaces\IAllowedOptionsAssert
     */
    protected function getTraitMock(){
        $m = $this->getMockForTrait('Aedart\ArrayOption\Traits\AllowedOptionsAssertTrait');
        return $m;
    }

    /**
     * Get a populated allowed options list mock
     *
     * @return Mockery\MockInterface|Aedart\ArrayOption\Interfaces\IAllowedOptionsList
     */
    protected function getAllowedOptionsListMock(){
        $m = Mockery::mock('Aedart\ArrayOption\AllowedOptionsList')->makePartial();
        $m->populate([
            [
                'name' => 'myOption',
                'type' => IOptionType::TYPE_STRING,
                'required' => false,
                'defaultValue'  => 'doIt',
                'description' => 'A single option with do-it'
            ],
            [
                'name' => 'myOtherOption',
                'type' => IOptionType::TYPE_INTEGER,
                'required' => false,
                'defaultValue'  => 27,
                'description' => 'Another custom option'
            ],
            [
                'name' => 'specialRequired',
                'type' => IOptionType::TYPE_BOOLEAN,
                'required' => true,
                'defaultValue'  => true,
                'description' => 'Another custom option'
            ],
            [
                'name' => 'LastOption',
                'type' => [IOptionType::TYPE_ARRAY, IOptionType::TYPE_FLOAT],
                'required' => false,
                'defaultValue'  => [1, 2, 3],
                'description' => 'Special...'
            ],
        ]);
        return $m;
    }

    /******************************************************************************
     * Tests
     *****************************************************************************/

    /**
     * @test
     * @covers ::assertOption
     *
     * NOTE: if no exception thrown, then test passes
     */
    public function assertOptionValid(){
        $trait = $this->getTraitMock();
        $allowedOptionsList = $this->getAllowedOptionsListMock();

        $trait->assertOption('myOption', 'something', $allowedOptionsList);

        $this->assertTrue(true);
    }

    /**
     * @test
     * @covers ::assertOption
     *
     * @expectedException \Aedart\ArrayOption\Exception\InvalidOptionNameException
     */
    public function assertOptionInvalidOptionName(){
        $trait = $this->getTraitMock();
        $allowedOptionsList = $this->getAllowedOptionsListMock();

        $trait->assertOption('my invalid.optionName', 22, $allowedOptionsList);
    }

    /**
     * @test
     * @covers ::assertOption
     *
     * @expectedException \Aedart\ArrayOption\Exception\UnknownOptionException
     */
    public function assertOptionNotAllowedOption(){
        $trait = $this->getTraitMock();
        $allowedOptionsList = $this->getAllowedOptionsListMock();

        $trait->assertOption('myUnknownOption', true, $allowedOptionsList);
    }

    /**
     * @test
     * @covers ::assertOption
     *
     * @expectedException \Aedart\ArrayOption\Exception\InvalidOptionTypeException
     */
    public function assertOptionInvalidValueDataType(){
        $trait = $this->getTraitMock();
        $allowedOptionsList = $this->getAllowedOptionsListMock();

        $trait->assertOption('LastOption', 'my incorrect msg', $allowedOptionsList);
    }

    /**
     * @test
     * @covers ::assertOptionsList
     *
     * NOTE: if no exceptions thrown, test passes
     */
    public function assertOptionsListValid(){
        $trait = $this->getTraitMock();
        $allowedOptionsList = $this->getAllowedOptionsListMock();

        $options = [
            'specialRequired' => false
        ];

        $trait->assertOptionsList($options, $allowedOptionsList);

        $this->assertTrue(true);
    }

    /**
     * @test
     * @covers ::assertOptionsList
     *
     * @expectedException \Aedart\ArrayOption\Exception\RequiredOptionException
     */
    public function assertOptionsListMissingRequiredOptions(){
        $trait = $this->getTraitMock();
        $allowedOptionsList = $this->getAllowedOptionsListMock();

        $options = [
            'myOption' => 'weee',
            'myOtherOption' => 19,
            'LastOption' => 11.5,
        ];

        $trait->assertOptionsList($options, $allowedOptionsList);
    }
}