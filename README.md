## Array-Option ##

Allows you to defined a set of options (array key-value pair), which must follow a set of predefined rules (allowed options)

Official website (https://bitbucket.org/aedart/array-option)

## Contents ##

[TOC]

## When to use this ##

If your component, class or method accepts options, in the form of an array (key-value pair) and you need to perform some kind of basic validation of those options, e.g. are required, data-type check and eventually also have a predefined default value.

**Warning** I advise you NOT to use this component, unless you really cannot specify [method arguments]( http://php.net/manual/en/functions.arguments.php) in any other way, than in the form of an [associative array]( http://php.net/manual/en/language.types.array.php).


## How to install ##

```
#!console

composer require aedart/array-option 1.* 
```

This package uses [composer](https://getcomposer.org/). If you do not know what that is or how it works, I recommend that you read a little about, before attempting to use this package.

## Quick start ##

The blow stated example show how to build a component, which accepts 3 options, each having a default value and a specific data-type.

```
#!php
<?php
use Aedart\ArrayOption\Traits\ArrayOptionsListTrait;
use Aedart\ArrayOption\AllowedOptionsList;
use Aedart\ArrayOption\Interfaces\IOptionType;

class MyComponent {

    use ArrayOptionsListTrait;
    
    public function __construct(){

        // Specify the allowed options
        // See Aedart\ArrayOption\Interfaces\IAllowedOption
        $allowedOptions = new AllowedOptionsList([
            [
                'name' => 'myOption',
                'type' => IOptionType::TYPE_STRING,
                'required' => true,
                'defaultValue'  => 'doIt',
                'description' => 'A single option with do-it'
            ],
            [
                'name' => 'myOtherOption',
                'type' => IOptionType::TYPE_INTEGER,
                'required' => false,
                'defaultValue'  => 27,
                'description' => 'Another custom option'
            ],
            [
                'name' => 'LastOption',
                'type' => IOptionType::TYPE_ARRAY,
                'required' => false,
                'defaultValue'  => [1, 2, 3],
                'description' => 'Special...'
            ],
        ]);

        // Set the allowed-options-list
        $this->getArrayOptionsList()->setAllowedOptionsList($allowedOptions);
    }

    public function doSomeThing(array $options){
        // Set the given options in the "options-list"
        $optionsList = $this->getArrayOptionsList();
        $optionsList->setArrayOptions($options); // Validates input, throws exceptions if something is wrong

        // Get the value OR default value for a given option
        $myOption = $optionsList->getValue('myOption');

        return $myOption;
    }

}

```

For additional information, please review this package’s interfaces as well as the unit tests.

## License ##

[BSD-3-Clause](http://spdx.org/licenses/BSD-3-Clause), Read the LICENSE file included in this package