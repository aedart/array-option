<?php  namespace Aedart\ArrayOption;
use Aedart\ArrayOption\Exception\InvalidOptionNameException;
use Aedart\ArrayOption\Interfaces\IAllowedOption;
use Aedart\ArrayOption\Interfaces\IAllowedOptionsList;
use Aedart\ArrayOption\Traits\OptionNameValidateTrait;
use Aedart\ArrayOption\Traits\ValueTypeValidateTrait;

/**
 * Class Allowed-Options-List
 *
 * @see IAllowedOptionsList
 *
 * @author Alin Eugen Deac <aedart@gmail.com>
 * @package Aedart\ArrayOption
 */
class AllowedOptionsList implements IAllowedOptionsList{

    use OptionNameValidateTrait,
        ValueTypeValidateTrait;

    /**
     * List of allowed option entries
     *
     * @var IAllowedOption[]
     */
    protected $allowedOptionsList = [];

    /**
     * Create new instance of a allowed options list
     *
     * @param IAllowedOption[]|array[][] $allowedOptions [Optional][Default []] Populate list
     *
     * @see populate()
     */
    public function __construct(array $allowedOptions = []){
        if(!empty($allowedOptions)){
            $this->populate($allowedOptions);
        }
    }

    /**
     * Populate this component via an array
     *
     * @param IAllowedOption[]|array[][] $data  Example<p>
     * <pre>
     * [
     *  new AllowedOption(...),
     *  new AllowedOption(...),
     *  new AllowedOption(...),
     * ]
     * </pre>
     * Or <pre>
     * [
     *  ['name' => 'my_option', 'type' => 'string', 'defaultValue' => 'ok', 'description' => 'This is my...'],
     *  ['name' => 'myOtherOption', 'type' => 'integer', 'defaultValue' => 25, 'description' => 'Also my...'],
     * ]
     * </pre>
     * </p>
     *
     * @see IAllowedOption
     *
     * @return void
     *
     * @throws \InvalidArgumentException In case that one or more of the given array entries are invalid
     */
    public function populate(array $data) {
        foreach($data as $key => $value){
            $allowedOption = $value;

            if(is_array($value)){
                $allowedOption = $this->createDefaultAllowedOption();
                $allowedOption->populate($value);
            }

            if($allowedOption instanceof IAllowedOption){
                $this->setAllowedOption($allowedOption);
            } else {
                throw new \InvalidArgumentException(sprintf('Instance of IAllowedOption|array was expected, "%s" was given in $data[%d]', var_export($allowedOption, true), $key));
            }
        }
    }

    /**
     * Create and return an empty allowed option
     *
     * @return AllowedOption New empty instance of an allowed option
     */
    protected function createDefaultAllowedOption(){
        return new AllowedOption();
    }

    public function setAllowedOption(IAllowedOption $option) {
        $optionName = $option->getName();
        $previousEntry = $this->getAllowedOption($optionName);
        $this->allowedOptionsList[$optionName] = $option;
        return $previousEntry;
    }

    public function getAllowedOption($optionName) {
        if($this->hasAllowedOption($optionName)){
            return $this->allowedOptionsList[$optionName];
        }
        return null;
    }

    public function hasAllowedOption($optionName) {
        if(!$this->isOptionNameValid($optionName)){
            throw new InvalidOptionNameException(sprintf('Option name "%s" is not allowed, cannot check if there is an allowed option with that name', $optionName));
        }

        return array_key_exists($optionName, $this->allowedOptionsList);
    }

    public function getAllowedOptions() {
        return $this->allowedOptionsList;
    }

    public function isOptionValid($name, $value) {
        try {
            // Try to fetch allowed option - provided that it exists
            // and the provided name is a "valid" option name
            $allowedOption = $this->getAllowedOption($name);
        } catch(InvalidOptionNameException $e){
            return false;
        }

        // Check if there was an allowed option found, with corresponding option-name
        if(is_null($allowedOption)){
            return false;
        }

        // Get the allowed value-type(s)
        $allowedTypes = $allowedOption->getType();

        // Finally, check if the type is allowed
        return $this->isValueTypeAllowed($value, $allowedTypes);
    }

    public function isOptionRequired($name){
        $requiredOptions = $this->getRequiredOptions();
        return array_key_exists($name, $requiredOptions);
    }

    public function getRequiredOptions(){
        $allowedOptionsList = $this->getAllowedOptions();
        $requiredOptions = [];

        foreach($allowedOptionsList as $allowedOption){
            if($allowedOption->isRequired()){
                $requiredOptions[$allowedOption->getName()] = $allowedOption;
            }
        }
        return $requiredOptions;
    }

    public function count() {
        return count($this->allowedOptionsList);
    }

    public function toArray() {
        $list = $this->getAllowedOptions();
        $output = [];

        foreach($list as $optionName => $option){
            $output[$optionName] = $option->toArray();
        }

        return $output;
    }
}