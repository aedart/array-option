<?php

use Aedart\ArrayOption\Interfaces\ArrayOptionsListAware;
use Aedart\ArrayOption\Traits\ArrayOptionsListTrait;
use \Mockery;

/**
 * Class ArrayOptionsTraitTest
 *
 * @coversDefaultClass Aedart\ArrayOption\Traits\ArrayOptionsListTrait
 *
 * @author Alin Eugen Deac <aedart@gmail.com>
 */
class ArrayOptionsTraitTest extends \Codeception\TestCase\Test
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
        Mockery::close();
    }

    /******************************************************************************
     * Providers
     *****************************************************************************/

    /**
     * Get a mock for the trait in question
     *
     * @return PHPUnit_Framework_MockObject_MockObject|Aedart\ArrayOption\Interfaces\ArrayOptionsListAware
     */
    protected function getTraitMock(){
        $m = $this->getMockForTrait('Aedart\ArrayOption\Traits\ArrayOptionsListTrait');
        return $m;
    }

    /**
     * Get allowed options list mock
     *
     * @return \Mockery\MockInterface|Aedart\ArrayOption\Interfaces\IArrayOptionsList
     */
    protected function getArrayOptionsListMock(){
        $m = Mockery::mock('Aedart\ArrayOption\ArrayOptionsList');
        return $m;
    }

    /******************************************************************************
     * Tests
     *****************************************************************************/

    /**
     * @test
     * @covers ::getArrayOptionsList
     * @covers ::setArrayOptionsList
     * @covers ::getDefaultArrayOptionsList
     */
    public function getDefaultAllowedOptionsList(){
        $trait = $this->getTraitMock();
        $this->assertInstanceOf('Aedart\ArrayOption\Interfaces\IArrayOptionsList', $trait->getArrayOptionsList());
    }

    /**
     * @test
     * @covers ::getArrayOptionsList
     * @covers ::setArrayOptionsList
     */
    public function setAndGetArrayOptionsList(){
        $trait = $this->getTraitMock();

        $arrayOptionsList = $this->getArrayOptionsListMock();

        $trait->setArrayOptionsList($arrayOptionsList);

        $this->assertSame($arrayOptionsList, $trait->getArrayOptionsList());
    }
}