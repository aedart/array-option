<?php  namespace Aedart\ArrayOption\Exception; 

/**
 * Class Invalid Option-Type Exception
 *
 * Throw this exception whenever an invalid option-type (data-type) has been provided
 *
 * @author Alin Eugen Deac <aedart@gmail.com>
 * @package Aedart\ArrayOption\Exception
 */
class InvalidOptionTypeException extends \InvalidArgumentException{

}