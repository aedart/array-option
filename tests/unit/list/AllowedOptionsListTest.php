<?php

use Aedart\ArrayOption\AllowedOptionsList;
use Aedart\ArrayOption\Interfaces\IOptionType;
use \Mockery;

/**
 * Class AllowedOptionsListTest
 *
 * @coversDefaultClass Aedart\ArrayOption\AllowedOptionsList
 *
 * @author Alin Eugen Deac <aedart@gmail.com>
 */
class AllowedOptionsListTest extends \Codeception\TestCase\Test
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
        Mockery::close();
    }

    /******************************************************************************
     * Providers
     *****************************************************************************/

    /**
     * Get an new empty list
     *
     * @return AllowedOptionsList
     */
    protected function getNewEmptyList(){
        return new AllowedOptionsList();
    }

    /**
     * Get allowed option mock
     *
     * @return Mockery\MockInterface|Aedart\ArrayOption\Interfaces\IAllowedOption
     */
    protected function getAllowedOptionMock(){
        $m = Mockery::mock('Aedart\ArrayOption\AllowedOption')->makePartial();
        return $m;
    }

    /**
     * Get some dummy populate data for an allowed option
     *
     * @return array
     */
    protected function getAllowedOptionDummyData(){
        return [
            'name' => 'myOption',
            'type' => IOptionType::TYPE_STRING,
            'required' => true,
            'defaultValue'  => 'doIt',
            'description' => 'A single option with do-it'
        ];
    }

    /**
     * Get a populate dummy data
     *
     * @return array
     */
    protected function getValidPopulateDataArr(){
        return [
            [
                'name' => 'myOption',
                'type' => IOptionType::TYPE_STRING,
                'required' => true,
                'defaultValue'  => 'doIt',
                'description' => 'A single option with do-it'
            ],
            [
                'name' => 'myOtherOption',
                'type' => IOptionType::TYPE_INTEGER,
                'required' => false,
                'defaultValue'  => 27,
                'description' => 'Another custom option'
            ],
            [
                'name' => 'LastOption',
                'type' => IOptionType::TYPE_ARRAY,
                'required' => false,
                'defaultValue'  => [1, 2, 3],
                'description' => 'Special...'
            ],
        ];
    }

    /**
     * Get a populate dummy data list with allowed options objects
     *
     * @return Aedart\ArrayOption\Interfaces\IAllowedOption[]
     */
    protected function getValidPopulateDataObjects(){
        $arrList = $this->getValidPopulateDataArr();

        $m1 = $this->getAllowedOptionMock();
        $m1->populate($arrList[0]);

        $m2 = $this->getAllowedOptionMock();
        $m2->populate($arrList[1]);

        $m3 = $this->getAllowedOptionMock();
        $m3->populate($arrList[2]);

        return [$m1, $m2, $m3];
    }

    /**
     * Data provider
     *
     * @return array
     */
    public function getDataForIsOptionValid(){
        $allowedOptions = $this->getValidPopulateDataArr();

        return [
            ['myOption', 'go go go', $allowedOptions, true],
            ['LastOption', [4, 5, 6], $allowedOptions, true],
            ['unknownOption', false, $allowedOptions, false],
            ['my invalid option name', false, $allowedOptions, false],
            ['myOtherOption', true, $allowedOptions, false],
        ];
    }

    /******************************************************************************
     * Tests
     *****************************************************************************/

    /**
     * @test
     * @covers ::hasAllowedOption
     */
    public function hasAllowedOptionInEmptyList(){
        $list = $this->getNewEmptyList();
        $this->assertFalse($list->hasAllowedOption('someOption'));
    }

    /**
     * @test
     * @covers ::hasAllowedOption
     *
     * @expectedException \Aedart\ArrayOption\Exception\InvalidOptionNameException
     */
    public function hasAllowedOptionInvalidName(){
        $list = $this->getNewEmptyList();
        $list->hasAllowedOption('bla bla bla');
    }

    /**
     * @test
     * @covers ::setAllowedOption
     * @covers ::getAllowedOption
     * @covers ::hasAllowedOption
     */
    public function setAndGetAllowedOption(){
        $list = $this->getNewEmptyList();

        $dummyPopulateData = $this->getAllowedOptionDummyData();

        $allowedOption = $this->getAllowedOptionMock();
        $allowedOption->populate($dummyPopulateData);

        $previousEntry = $list->setAllowedOption($allowedOption);

        $this->assertSame($allowedOption, $list->getAllowedOption($dummyPopulateData['name']), 'Allowed option should exist inside list');
        $this->assertNull($previousEntry, 'There should not had been any previous entry!');
    }

    /**
     * @test
     * @covers ::setAllowedOption
     * @covers ::getAllowedOption
     * @covers ::hasAllowedOption
     */
    public function setAndGetPreviousAllowedOption(){
        $list = $this->getNewEmptyList();

        $firstDummyPopulateData = $this->getAllowedOptionDummyData();
        $firstDummyPopulateData['defaultValue'] = 'first';

        $secondDummyPopulateData = $this->getAllowedOptionDummyData();
        $secondDummyPopulateData['defaultValue'] = 'second';

        $firstAllowedOption = $this->getAllowedOptionMock();
        $firstAllowedOption->populate($firstDummyPopulateData);

        $secondAllowedOption = $this->getAllowedOptionMock();
        $secondAllowedOption->populate($secondDummyPopulateData);

        // expected null
        $firstPreviousEntry = $list->setAllowedOption($firstAllowedOption);
        $secondPreviousEntry = $list->setAllowedOption($secondAllowedOption);

        $this->assertNull($firstPreviousEntry, 'Should be null');
        $this->assertSame($firstAllowedOption, $secondPreviousEntry, 'Previous entry should had matched the first allowedd option added');
    }

    /**
     * @test
     * @covers ::getAllowedOption
     */
    public function getNoneExistingAllowedOption(){
        $list = $this->getNewEmptyList();
        $this->assertNull($list->getAllowedOption('someOption'));
    }

    /**
     * @test
     * @covers ::__construct
     * @covers ::populate
     * @covers ::createDefaultAllowedOption
     * @covers ::count
     */
    public function createInstanceWithPopulateData(){
        $dummyArr = $this->getValidPopulateDataArr();
        $list = new AllowedOptionsList($dummyArr);

        $this->assertSame(3, $list->count(), 'Invalid count added');
        $this->assertTrue($list->hasAllowedOption($dummyArr[0]['name']));
    }

    /**
     * @test
     * @covers ::populate
     * @covers ::count
     */
    public function populateWithObjectInstances(){
        $dummyArr = $this->getValidPopulateDataObjects();
        $list = $this->getNewEmptyList();
        $list->populate($dummyArr);

        $this->assertSame(3, $list->count(), 'Invalid count added');
        $this->assertTrue($list->hasAllowedOption($dummyArr[2]->getName()));
    }

    /**
     * @test
     * @covers ::populate
     *
     * @expectedException InvalidArgumentException
     */
    public function attemptPopulateWithInvalidData(){
        $list = $this->getNewEmptyList();
        $list->populate([false, false, true]);
    }

    /**
     * @test
     * @covers ::getAllowedOptions
     * @covers ::toArray
     */
    public function toArray(){
        $dummyArr = $this->getValidPopulateDataArr();
        $dummyArrObj = $this->getValidPopulateDataObjects();
        $list = $this->getNewEmptyList();
        $list->populate($dummyArrObj);

        $exportedList = $list->toArray();

        $this->assertSame(count($dummyArr), count($exportedList), 'Exported array has not same amount of entries as expected');

        foreach($exportedList as $allowedOption){
            $this->assertTrue(in_array($allowedOption, $dummyArr), sprintf('%s is not inside %s', print_r($allowedOption, true), print_r($dummyArr, true)));
        }
    }

    /**
     * @test
     * @covers ::isOptionValid
     *
     * @dataProvider getDataForIsOptionValid
     *
     * @param string $name
     * @param mixed $value
     * @param array $allowedOptions
     * @param bool $expectedResult
     */
    public function isOptionValid($name, $value, array $allowedOptions, $expectedResult){
        $list = $this->getNewEmptyList();
        $list->populate($allowedOptions);

        $this->assertSame($expectedResult, $list->isOptionValid($name, $value));
    }

    /**
     * @test
     * @covers ::getRequiredOptions
     */
    public function getRequiredOptions(){
        $dummyArrObj = $this->getValidPopulateDataObjects();
        $list = $this->getNewEmptyList();
        $list->populate($dummyArrObj);

        $this->assertSame(1, count($list->getRequiredOptions()));
    }

    /**
     * @test
     * @covers ::getRequiredOptions
     * @covers ::isOptionRequired
     */
    public function isOptionRequired(){
        $dummyArrObj = $this->getValidPopulateDataObjects();
        $list = $this->getNewEmptyList();
        $list->populate($dummyArrObj);

        $this->assertFalse($list->isOptionRequired('LastOption'), 'Last option should not be required');
        $this->assertTrue($list->isOptionRequired('myOption'), 'myOption should be required');
    }
}