<?php  namespace Aedart\ArrayOption\Interfaces;

use Aedart\ArrayOption\Exception\InvalidOptionDescriptionException;
use Aedart\ArrayOption\Exception\InvalidOptionNameException;
use Aedart\ArrayOption\Exception\InvalidOptionRequiredFlagException;
use Aedart\ArrayOption\Exception\InvalidOptionTypeException;
use Aedart\ArrayOption\Exception\InvalidOptionValueException;
use Aedart\ArrayOption\Interfaces\IOptionType;
use Aedart\Util\Interfaces\Populatable;
use Illuminate\Contracts\Support\Arrayable;

/**
 * Interface Allowed Option
 *
 * Object that contains information about a given "allowed" option
 *
 * @author Alin Eugen Deac <aedart@gmail.com>
 * @package Aedart\ArrayOption\Interfaces
 */
interface IAllowedOption extends Populatable, Arrayable{

    /**
     * Set this allowed option's name
     *
     * @param string $name Name of this allowed option
     *
     * @return void
     *
     * @throws InvalidOptionNameException If the name is invalid
     */
    public function setName($name);

    /**
     * Get this allowed option's name
     *
     * @return string Name of this allowed option
     */
    public function getName();

    /**
     * Set this allowed option's type (data-type)
     *
     * Multiple types can be specified by providing an array of allowed types
     *
     * @see IOptionType
     *
     * @param string|string[] $type [Optional][Default IOptionType::TYPE_NULL] The allowed data-type(s), example; <p>
     *      <pre>
     *          [IOptionType::TYPE_STRING, IOptionType::TYPE_OBJECT, IOptionType::TYPE_BOOLEAN]
     *      </pre>
     * </p>
     *
     * @return void
     *
     * @throws InvalidOptionTypeException If given type(s) are not allowed / supported or unknown
     */
    public function setType($type = IOptionType::TYPE_NULL);

    /**
     * Get the allowed option's data-type(s)
     *
     * @see IOptionType
     *
     * @return string|string[] The allowed data-type(s)
     */
    public function getType();

    /**
     * Set this allowed option's required state - if it is required or not
     *
     * @param bool $isRequired Require state/flag - True if this option is required, false if not
     *
     * @return void
     *
     * @throws InvalidOptionRequiredFlagException If given value is not a valid boolean
     */
    public function setRequired($isRequired);

    /**
     * Get this allowed option's required state - if it is required or not
     *
     * @return bool Require state/flag - True if this option is required, false if not
     */
    public function getRequired();

    /**
     * Alias for getRequired
     *
     * @see getRequired()
     *
     * @return bool Require state/flag - True if this option is required, false if not
     */
    public function isRequired();

    /**
     * Set this allowed option's default value
     *
     * @param mixed $value [Optional][Default null] A default value to be used
     *
     * @return void
     *
     * @throws InvalidOptionValueException If given value is of a invalid type (data-type), e.g. the allowed
     * type might be specified as a 'string', but an integer has been provided
     */
    public function setDefaultValue($value = null);

    /**
     * Get this allowed option's default value
     *
     * @return mixed A default value to be used
     */
    public function getDefaultValue();

    /**
     * Set this allowed option's description - what it is, or what it is going
     * to be used for
     *
     * @param string $desc [Optional][default ''] A short description of this allowed option
     *
     * @return void
     *
     * @throws InvalidOptionDescriptionException If given description is invalid, e.g. not a string or too long
     */
    public function setDescription($desc = '');

    /**
     * Get this allowed option's description - what it is, or what it is going
     * to be used for
     *
     * @return string A short description of this allowed option
     */
    public function getDescription();
}