<?php  namespace Aedart\ArrayOption\Interfaces;

use Aedart\ArrayOption\Exception\InvalidOptionNameException;
use Aedart\ArrayOption\Exception\InvalidOptionTypeException;
use Aedart\ArrayOption\Exception\RequiredOptionException;
use Aedart\ArrayOption\Exception\UnknownOptionException;
use Aedart\ArrayOption\Interfaces\IAllowedOptionsList;
use Illuminate\Contracts\Support\Arrayable;

/**
 * Interface Array-Options-List
 *
 * <br />
 *
 * <p>
 * Components that implement this interface, must be able to provide ways to define
 * a set of allowed options, as well as setting and obtaining those actual options.
 * </p>
 *
 * <br />
 *
 * <p>
 * Each time options are being set, this list ensures that every given option follows
 * the predefined set of rules (the allowed options).
 * </p>
 *
 * <br />
 *
 * <b>Allowed Options</b><p>
 * This type can be seen as a <i>"schema"</i>, containing information about an option's name,
 * allowed value's data-type, eventual predefined value, and a small description about the
 * what the option does
 * </p>
 *
 * <br />
 *
 * <b>Options (actual options)</b><p>
 * This is the actual options as it is being given.
 * </p>
 *
 * @see IAllowedOptionsList
 *
 * @author Alin Eugen Deac <aedart@gmail.com>
 * @package Aedart\ArrayOption\Interfaces
 */
interface IArrayOptionsList extends AllowedOptionsListAware, \Countable, Arrayable{

    /**
     * Set a set/list of options
     *
     * <b>Warning</b><p>
     * This method will assert all the given options, and throw exceptions, if
     * the following conditions are not fulfilled;
     * <ul>
     *  <li>All required options must included in the given array-options</li>
     *  <li>Option-name(s) must be valid</li>
     *  <li>The given options must be allowed (must be defined as an allowed option)</li>
     *  <li>Each option-value's data-type must correspond to the defined/allowed option's set of supported data-types</li>
     * </ul>
     * </p>
     *
     * @param array $options [Optional][Default []] Options, Key value pair, where the key
     * corresponds to the option-name, and the value corresponds to the option-value
     *
     * @return void
     *
     * @throws RequiredOptionException If a required option was not given (not part of the provided array-options)
     * @throws InvalidOptionNameException If a given option name is invalid - not allowed as an option name
     * @throws UnknownOptionException If given option is unknown / unsupported
     * @throws InvalidOptionTypeException If data-type of given value is not supported / allowed
     */
    public function setArrayOptions(array $options = []);

    /**
     * Get the array-options list, populated with eventual default values,
     * for those options that were not set / specified
     *
     * <b>Example</b><p>
     * If you define a set of allowed-options, with default values, those
     * options and values will be returned by this method, <b>if and only if</b>
     * the given options has not been set / overridden with a custom
     * value.
     * </p>
     *
     * @see setArrayOptions()
     *
     * @return array List of array-options with default values, as specified in
     * the allowed-options, if no custom value was set
     */
    public function getArrayOptions();

    /**
     * Get the list of array-options, as they have been provided,
     * without any modifications; without eventual preset default
     * values
     *
     * @see getArrayOptions()
     *
     * @return array List of the given array-options, untouched / not modified
     * OR empty array if no options was set
     */
    public function getCleanArrayOptions();

    /**
     * Get the value or default-value for the given option
     *
     * This method ensures to return the value, as it has been
     * provided when the array-options had been set <b>Or</b> a
     * default-value from the allowed-options, if no option with
     * the given name was provided!
     *
     * @param string $optionName The option (name) whose value needs to be retrieved
     *
     * @see IAllowedOptionsList
     * @see getAllowedOptionsList()
     *
     * @return mixed The given option value as provided when options has been set OR
     * a default option value, if such exists for the given option and no value was given
     *
     * @throws InvalidOptionNameException If the given option name is invalid
     * @throws UnknownOptionException If there is no option/allowed-option with that given name
     */
    public function getValue($optionName);

    /**
     * Check if this array-option-list has any defined allowed options
     *
     * If there are no defined options, none this entire list will not be
     * useful for anything! In other words, no options are allowed / supported
     *
     * @return bool True if this array options list has defined a set of allowed options, false if not
     */
    public function doesAllowOptions();

    /**
     * Check if this list has a set of defined options, which must be
     * provided
     *
     * @return bool True if there are defined allowed options which are required, false if not
     */
    public function doesRequireOptions();
}