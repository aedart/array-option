<?php

use Aedart\ArrayOption\Traits\OptionNameValidateTrait;

/**
 * Class OptionNameValidateTraitTest
 *
 * @coversDefaultClass Aedart\ArrayOption\Traits\OptionNameValidateTrait
 *
 * @author Alin Eugen Deac <aedart@gmail.com>
 */
class OptionNameValidateTraitTest extends \Codeception\TestCase\Test
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    /******************************************************************************
     * Providers
     *****************************************************************************/

    /**
     * Get a mock for the trait in question
     *
     * @return Aedart\ArrayOption\Traits\OptionNameValidateTrait
     */
    protected function getTraitMock(){
        $m = $this->getMockForTrait('Aedart\ArrayOption\Traits\OptionNameValidateTrait');
        return $m;
    }

    /******************************************************************************
     * Tests
     *****************************************************************************/

    /**
     * @test
     * @covers ::isOptionNameValid
     */
    public function validOptionName(){
        $trait = $this->getTraitMock();

        $optionName = 'my_option-Name';

        $this->assertTrue($trait->isOptionNameValid($optionName));
    }

    /**
     * @test
     * @covers ::isOptionNameValid
     */
    public function invalidOptionName(){
        $trait = $this->getTraitMock();

        $optionName = 'my option Name';

        $this->assertFalse($trait->isOptionNameValid($optionName));
    }
}