<?php  namespace Aedart\ArrayOption\Interfaces; 

use Aedart\ArrayOption\Exception\InvalidOptionNameException;
use Aedart\ArrayOption\Interfaces\IAllowedOption;
use Aedart\ArrayOption\Interfaces\IOptionType;
use Aedart\Util\Interfaces\Populatable;
use Illuminate\Contracts\Support\Arrayable;

/**
 * Interface Allowed Options List
 *
 * A list of allowed options is nothing more than a "mini" map, which contains entries
 * that define a set of basic rules for "options", such as; option name, option data-type,
 * is it required, and possible also a short description.
 *
 * This kind of list also provides simple methods for testing / validating a given
 * option name and value, by comparing those to the set of allowed options, which the
 * list contains.
 *
 * @see IAllowedOption
 *
 * @author Alin Eugen Deac <aedart@gmail.com>
 * @package Aedart\ArrayOption\Interfaces
 */
interface IAllowedOptionsList extends Populatable, Arrayable, \Countable{

    /**
     * Set (or add) an allowed option
     *
     * If there is no allowed option set, with the given allowed option's name,
     * then it will be set. However, if there already exists an option with
     * the same name, then that allowed option will be overridden and the previous
     * allowed option will be returned
     *
     * @param IAllowedOption $option The allowed option
     *
     * @return IAllowedOption|null The previous allowed option, if it is being overridden OR null if there was no previous option,
     * with that given name
     */
    public function setAllowedOption(IAllowedOption $option);

    /**
     * Get the allowed option which has the given name
     *
     * @param string $optionName The option name to search for and return it's
     * corresponding allowed option-entry
     *
     * @return IAllowedOption|null The allowed option OR null if there is no allowed option with the given name
     *
     * @throws InvalidOptionNameException If the given name is invalid, e.g. not a string or empty
     */
    public function getAllowedOption($optionName);

    /**
     * Check if this list contains an allowed option with the given name
     *
     * @param string $optionName The option name to search for
     *
     * @return bool True if this list contains an allowed option with the given name, false if not
     *
     * @throws InvalidOptionNameException If the given name is invalid, e.g. not a string or empty
     */
    public function hasAllowedOption($optionName);

    /**
     * Get the set allowed-options
     *
     * @return IAllowedOption[] List of set allowed-options entries
     */
    public function getAllowedOptions();

    /**
     * Validate the given option name and value
     *
     * This method searches this allowed-option's list; if it finds an allowed option
     * with the same name, then it proceeds to check if the given value is allowed / supported,
     * by comparing the given value to the allowed-option's type(s)
     *
     * @see IAllowedOption
     * @see IOptionType
     *
     * @param string $name The option name to be validated (is it allowed or not)
     * @param mixed $value The option value to be validated (is the data-type allowed or not)
     *
     * @return bool True if the given name and value are allowed, false if not
     */
    public function isOptionValid($name, $value);

    /**
     * Check if the given option is required - must be specified
     *
     * This method compares the set/added allowed options for the
     * given option-name and check's if the option has the 'required'
     * state/flag set to true. If that's the case, then the given
     * option is required (in the given context of usage)
     *
     * @param string $name The option name to be checked, if its required
     *
     * @return bool True if the given option is required, false if not
     */
    public function isOptionRequired($name);

    /**
     * Get a list of those allowed options, which are required
     *
     * @return IAllowedOption[] List of set allowed-options entries OR empty array if there are no required options
     */
    public function getRequiredOptions();
}