<?php  namespace Aedart\ArrayOption\Traits;
use Aedart\ArrayOption\Interfaces\IOptionType;

/**
 * Trait Option-Type Validate
 *
 * @author Alin Eugen Deac <aedart@gmail.com>
 * @package Aedart\ArrayOption\Traits
 */
trait OptionTypeValidateTrait {

    /**
     * Check if the given type(s) are valid or not
     *
     * <b>WARNING:</b> This method doesn't actually check the given data-type, only
     * is the given type name is allowed!
     *
     * @param string|string[] $type The type(s) to be validated
     *
     * @return bool True if the given type(s) are valid, false if not
     */
    public function isOptionTypeValid($type){
        if(is_string($type)){
            return $this->isStrOptionTypeValid($type);
        } else if(is_array($type)){
            return $this->isArrOptionTypeValid($type);
        } else {
            return false;
        }
    }

    /**
     * Check if the given type(s) are valid or not
     *
     * @param array $types List of types to be validated
     *
     * @return bool True if the given type(s) are valid, false if not
     */
    protected function isArrOptionTypeValid(array $types){
        foreach($types as $type){
            if(!$this->isStrOptionTypeValid($type)){
                return false;
            }
        }

        return true;
    }

    /**
     * Check if the given type is valid or not
     *
     * @param mixed $type The type to be validated
     *
     * @return bool True if the given type is allowed, false if not
     */
    protected function isStrOptionTypeValid($type){
        $t = strtolower($type);

        switch($t){
            case IOptionType::TYPE_ARRAY:
            case IOptionType::TYPE_BOOLEAN:
            case IOptionType::TYPE_FLOAT:
            case IOptionType::TYPE_INTEGER:
            case IOptionType::TYPE_NULL:
            case IOptionType::TYPE_OBJECT:
            case IOptionType::TYPE_RESOURCE:
            case IOptionType::TYPE_STRING:
                return true;

            default:
                return false;
        }
    }
}