<?php

use Aedart\ArrayOption\ArrayOptionsList;
use Aedart\ArrayOption\AllowedOptionsList;
use Aedart\ArrayOption\Interfaces\IOptionType;

/**
 * Class ArrayOptionListTest
 *
 * @coversDefaultClass Aedart\ArrayOption\ArrayOptionsList
 *
 * @author Alin Eugen Deac <aedart@gmail.com>
 */
class ArrayOptionsListTest extends \Codeception\TestCase\Test
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    /******************************************************************************
     * Providers
     *****************************************************************************/

    /**
     * Get a new empty options list
     *
     * @return ArrayOptionsList
     */
    protected function getEmptyArrayOptionsList(){
        return new ArrayOptionsList();
    }

    /**
     * Get an new populated options list - (allowed options defined)
     *
     * @return ArrayOptionsList
     */
    protected function getArrayOptionsList(){
        $optionsList = new ArrayOptionsList();
        $optionsList->setAllowedOptionsList($this->getAllowedOptionsList());
        return $optionsList;
    }

    /**
     * Get an allowed options list
     *
     * @return AllowedOptionsList
     */
    protected function getAllowedOptionsList(){
        return new AllowedOptionsList($this->getValidPopulateDataArr());
    }

    /**
     * Get a populate dummy data
     *
     * @return array
     */
    protected function getValidPopulateDataArr(){
        return [
            [
                'name' => 'myOption',
                'type' => IOptionType::TYPE_STRING,
                'required' => true,
                'defaultValue'  => 'doIt',
                'description' => 'A single option with do-it'
            ],
            [
                'name' => 'myOtherOption',
                'type' => IOptionType::TYPE_INTEGER,
                'required' => false,
                'defaultValue'  => 27,
                'description' => 'Another custom option'
            ],
            [
                'name' => 'LastOption',
                'type' => IOptionType::TYPE_ARRAY,
                'required' => false,
                'defaultValue'  => [1, 2, 3],
                'description' => 'Special...'
            ],
        ];
    }

    /******************************************************************************
     * Tests
     *****************************************************************************/

    /**
     * @test
     * @covers ::count
     */
    public function countOptions(){
        $optionsList = $this->getArrayOptionsList();
        $this->assertSame(count($this->getValidPopulateDataArr()), $optionsList->count());
    }

    /**
     * @test
     * @covers ::doesAllowOptions
     */
    public function doesAllowOptions(){
        $optionsList = $this->getArrayOptionsList();
        $this->assertTrue($optionsList->doesAllowOptions());
    }

    /**
     * @test
     * @covers ::doesAllowOptions
     */
    public function doesNotAllowOptions(){
        $optionsList = $this->getEmptyArrayOptionsList();
        $this->assertFalse($optionsList->doesAllowOptions());
    }

    /**
     * @test
     * @covers ::doesRequireOptions
     */
    public function doesRequireOptions(){
        $optionsList = $this->getArrayOptionsList();
        $this->assertTrue($optionsList->doesRequireOptions());
    }

    /**
     * @test
     * @covers ::doesRequireOptions
     */
    public function doesNotRequireOptions(){
        $optionsList = $this->getEmptyArrayOptionsList();
        $this->assertFalse($optionsList->doesRequireOptions());
    }

    /**
     * @test
     * @covers ::setArrayOptions
     * @covers ::getArrayOptions
     * @covers ::hasArrayOption
     * @covers ::getValue
     * @covers ::getArrayOptionValue
     * @covers ::getCleanArrayOptions
     * @covers ::toArray
     */
    public function setAndGetArrayOptions(){
        $optionsList = $this->getArrayOptionsList();

        $options = [
            'myOption' => 'I am doing it',
            'LastOption' => [5, 6, 9]
        ];

        $optionsList->setArrayOptions($options);

        $arrOptions = $optionsList->toArray();

        $this->assertCount(count($this->getValidPopulateDataArr()), $arrOptions, 'Invalid amount of options returned');
        $this->assertSame($options['myOption'], $arrOptions['myOption'], 'Provided value does not match set value');
        $this->assertSame($options['LastOption'], $arrOptions['LastOption'], '2nd Provided value does not match set value');
        $this->assertSame($this->getValidPopulateDataArr()[1]['defaultValue'], $arrOptions['myOtherOption'], 'Default option / value does not match');
    }

    /**
     * @test
     * @covers ::setArrayOptions
     *
     * @expectedException \Aedart\ArrayOption\Exception\UnknownOptionException
     */
    public function attemptSetOptionsWithNotAllowedOption(){
        $optionsList = $this->getArrayOptionsList();

        $options = [
            'myOption' => 'I am doing it',
            'LastOption' => [5, 6, 9],
            'myNotAllowedOrSupportedOption' => false
        ];

        $optionsList->setArrayOptions($options);
    }

    /**
     * @test
     * @covers ::setArrayOptions
     *
     * @expectedException \Aedart\ArrayOption\Exception\RequiredOptionException
     */
    public function attemptSetOptionsWithMissingRequiredOption(){
        $optionsList = $this->getArrayOptionsList();

        $options = [
            'LastOption' => [5, 6, 9],
        ];

        $optionsList->setArrayOptions($options);
    }

    /**
     * @test
     * @covers ::setArrayOptions
     * @covers ::getValue
     * @covers ::hasArrayOption
     * @covers ::getArrayOptionValue
     *
     * @expectedException \Aedart\ArrayOption\Exception\UnknownOptionException
     */
    public function attemptGetValueForUnknownOption(){
        $optionsList = $this->getArrayOptionsList();

        $options = [
            'myOption' => 'I am doing it',
            'LastOption' => [5, 6, 9]
        ];

        $optionsList->setArrayOptions($options);

        $v = $optionsList->getValue('myUnknownOption');
    }
}