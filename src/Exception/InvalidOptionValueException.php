<?php  namespace Aedart\ArrayOption\Exception; 

/**
 * Class Invalid Option-Value Exception
 *
 * Throw this exception if a provided option value is invalid
 *
 * @author Alin Eugen Deac <aedart@gmail.com>
 * @package Aedart\ArrayOption\Exception
 */
class InvalidOptionValueException extends \InvalidArgumentException{

}