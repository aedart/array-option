<?php  namespace Aedart\ArrayOption\Traits;

use Aedart\ArrayOption\Interfaces\IArrayOptionsList;
use Aedart\ArrayOption\ArrayOptionsList;

/**
 * Trait Array Options
 *
 * @see ArrayOptionsListAware
 *
 * @author Alin Eugen Deac <aedart@gmail.com>
 * @package Aedart\ArrayOption\Traits
 */
trait ArrayOptionsListTrait {

    /**
     * Options list
     *
     * @var IArrayOptionsList|null
     */
    protected $arrayOptionsList = null;

    /**
     * Set this component's options list
     *
     * @param IArrayOptionsList $list List of options
     *
     * @return void
     */
    public function setArrayOptionsList(IArrayOptionsList $list){
        $this->arrayOptionsList = $list;
    }

    /**
     * Get this component's options list
     *
     * If no options list has been set, this method obtains, sets and
     * returns a default options list
     *
     * @see getDefaultArrayOptionsList()
     *
     * @return IArrayOptionsList List of options
     */
    public function getArrayOptionsList(){
        if(is_null($this->arrayOptionsList)){
            $this->setArrayOptionsList($this->getDefaultArrayOptionsList());
        }
        return $this->arrayOptionsList;
    }

    /**
     * Get a default options list instance
     *
     * @return IArrayOptionsList A default instance of a "options-list"
     */
    public function getDefaultArrayOptionsList(){
        return new ArrayOptionsList();
    }

}