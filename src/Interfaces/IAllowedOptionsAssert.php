<?php  namespace Aedart\ArrayOption\Interfaces; 

use Aedart\ArrayOption\Exception\InvalidOptionNameException;
use Aedart\ArrayOption\Exception\InvalidOptionTypeException;
use Aedart\ArrayOption\Exception\RequiredOptionException;
use Aedart\ArrayOption\Exception\UnknownOptionException;
use Aedart\ArrayOption\Interfaces\IAllowedOptionsList;

/**
 * Interface Allowed Options Assert
 *
 * Components that implement this interface, promise that a list of given array-options
 * can be asserted; tested if they are allowed / supported
 *
 * <b>Note</b>: This interface has nothing to do with a PHPUnit assert!
 *
 * @see IAllowedOptionsList
 *
 * @author Alin Eugen Deac <aedart@gmail.com>
 * @package Aedart\ArrayOption\Interfaces
 */
interface IAllowedOptionsAssert {

    /**
     * Assert the given array-options
     *
     * The method checks the following;
     * <ul>
     *  <li>If required options are included in the given array-options</li>
     *  <li>If each option-name is valid</li>
     *  <li>If each option is allowed - part of the allowed-options list</li>
     *  <li>If each option's value data-type is allowed / supported</li>
     * </ul>
     *
     * @param array $options The options to be asserted / tested
     * @param IAllowedOptionsList $allowedOptionsList The list of allowed-options, which is compared against
     * the given options
     *
     * @see assertOption()
     *
     * @return void
     *
     * @throws RequiredOptionException If a required option was not given (not part of the provided array-options)
     * @throws InvalidOptionNameException If a given option name is invalid - not allowed as an option name
     * @throws UnknownOptionException If given option is unknown / unsupported
     * @throws InvalidOptionTypeException If data-type of given value is not supported / allowed
     */
    public function assertOptionsList(array $options, IAllowedOptionsList $allowedOptionsList);

    /**
     * Assert the given option
     *
     * This method checks the following;
     * <ul>
     *  <li>If the option-name valid</li>
     *  <li>If the option is allowed (part of the allowed options list)</li>
     *  <li>If the value data-type is allowed / supported</li>
     * </ul>
     *
     * @param string $name The option name
     * @param mixed $value The option value
     * @param IAllowedOptionsList $allowedOptionsList The list of allowed options, in which the given option
     * name and value will be tested for - if its allowed and if its data-type is supported
     *
     * @return void
     *
     * @throws InvalidOptionNameException If the given name is invalid - not allowed as an option name
     * @throws UnknownOptionException If given option is unknown / unsupported
     * @throws InvalidOptionTypeException If data-type of given value is not supported / allowed
     */
    public function assertOption($name, $value, IAllowedOptionsList $allowedOptionsList);

}