<?php  namespace Aedart\ArrayOption\Traits; 

use Aedart\ArrayOption\Exception\InvalidOptionTypeException;
use Aedart\ArrayOption\Exception\RequiredOptionException;
use Aedart\ArrayOption\Exception\UnknownOptionException;
use Aedart\ArrayOption\Interfaces\IAllowedOptionsList;

/**
 * Trait Allowed Options Assert
 *
 * @see IAllowedOptionsAssert
 *
 * @author Alin Eugen Deac <aedart@gmail.com>
 * @package Aedart\ArrayOption\Traits
 */
trait AllowedOptionsAssertTrait {
    /**
     * Assert the given array-options
     *
     * The method checks the following;
     * <ul>
     *  <li>If required options are included in the given array-options</li>
     *  <li>If each option-name is valid</li>
     *  <li>If each option is allowed - part of the allowed-options list</li>
     *  <li>If each option's value data-type is allowed / supported</li>
     * </ul>
     *
     * @param array $options The options to be asserted / tested
     * @param IAllowedOptionsList $allowedOptionsList The list of allowed-options, which is compared against
     * the given options
     *
     * @see assertOption()
     *
     * @return void
     *
     * @throws RequiredOptionException If a required option was not given (not part of the provided array-options)
     * @throws InvalidOptionNameException If a given option name is invalid - not allowed as an option name
     * @throws UnknownOptionException If given option is unknown / unsupported
     * @throws InvalidOptionTypeException If data-type of given value is not supported / allowed
     */
    public function assertOptionsList(array $options, IAllowedOptionsList $allowedOptionsList){
        // A) Get list of required options - if any are...
        $requiredOptionsList = $allowedOptionsList->getRequiredOptions();
        foreach($requiredOptionsList as $requiredOption){
            if(!array_key_exists($requiredOption->getName(), $options)){
                throw new RequiredOptionException(sprintf('Option "%s" was expected but not part of the given options', $requiredOption->getName()));
            }
        }

        // B) Foreach array-option, assert it
        foreach($options as $optionName => $optionValue){
            $this->assertOption($optionName, $optionValue, $allowedOptionsList);
        }
    }

    /**
     * Assert the given option
     *
     * This method checks the following;
     * <ul>
     *  <li>If the option-name valid</li>
     *  <li>If the option is allowed (part of the allowed options list)</li>
     *  <li>If the value data-type is allowed / supported</li>
     * </ul>
     *
     * @param string $name The option name
     * @param mixed $value The option value
     * @param IAllowedOptionsList $allowedOptionsList The list of allowed options, in which the given option
     * name and value will be tested for - if its allowed and if its data-type is supported
     *
     * @return void
     *
     * @throws InvalidOptionNameException If the given name is invalid - not allowed as an option name
     * @throws UnknownOptionException If given option is unknown / unsupported
     * @throws InvalidOptionTypeException If data-type of given value is not supported / allowed
     */
    public function assertOption($name, $value, IAllowedOptionsList $allowedOptionsList){
        // A & B) assert that the name is valid and that the option is supported
        // ALSO - method throws InvalidOptionNameException, if name is invalid
        if(!$allowedOptionsList->hasAllowedOption($name)){
            throw new UnknownOptionException(sprintf('Option "%s" is unknown or not supported', $name));
        }

        // C) Check if the option is valid; if the data-type is allowed / supported
        // NOTE: The isOptionValid method also performs a test if the option-name is
        // part of the allowed-list. Nevertheless, we just need that method to ensure that
        // the data-type is supported / allowed, because we already tested is the option-name
        // is among the list of allowed options
        if(!$allowedOptionsList->isOptionValid($name, $value)){
            $allowedOption = $allowedOptionsList->getAllowedOption($name);
            $supportedTypes = $allowedOption->getType();
            if(is_array($supportedTypes)){
                $supportedTypes = implode('|', $supportedTypes);
            }

            $a = $supportedTypes . ' ' . $name;
            $b = gettype($value) . ' ' . var_export($value, true);

            $m = sprintf('Option "%s" does not allow values of the given data-type, ("%s" was given)', $a, $b);
            throw new InvalidOptionTypeException($m);
        }
    }
}