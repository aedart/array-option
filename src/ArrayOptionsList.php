<?php  namespace Aedart\ArrayOption;

use Aedart\ArrayOption\Exception\UnknownOptionException;
use Aedart\ArrayOption\Interfaces\IArrayOptionsList;
use Aedart\ArrayOption\Traits\AllowedOptionsAssertTrait;
use Aedart\ArrayOption\Traits\AllowedOptionsListTrait;

/**
 * Class Array Option List
 *
 * @see IArrayOptionsList
 *
 * @author Alin Eugen Deac <aedart@gmail.com>
 * @package Aedart\ArrayOption
 */
class ArrayOptionsList implements IArrayOptionsList{

    use AllowedOptionsListTrait,
        AllowedOptionsAssertTrait;

    /**
     * The array options - WITHOUT any modification,
     * as they have been set / given
     *
     * @see setArrayOptions()
     *
     * @var array
     */
    protected $arrayOptionsList = [];

    public function setArrayOptions(array $options = []){
        $this->assertOptionsList($options, $this->getAllowedOptionsList());
        $this->arrayOptionsList = $options;
    }

    public function getArrayOptions(){
        $allowedOptions = $this->getAllowedOptionsList()->getAllowedOptions();

        $output = [];

        foreach($allowedOptions as $name => $allowedOption){
            $output[$name] = $this->getValue($name);
        }

        return $output;
    }

    /**
     * Check if a given option has been set
     *
     * @param string $optionName
     *
     * @return bool True if the given option exists in the array-options list
     */
    protected function hasArrayOption($optionName){
        return array_key_exists($optionName, $this->getCleanArrayOptions());
    }

    /**
     * Get a given set option value, has it has been provided
     *
     * @param string $optionName
     *
     * @return mixed The user-given option value
     */
    protected function getArrayOptionValue($optionName){
        return $this->getCleanArrayOptions()[$optionName];
    }

    public function getCleanArrayOptions(){
        return $this->arrayOptionsList;
    }

    public function getValue($optionName){
        // Fet the allowed options list
        $allowedOptionsList = $this->getAllowedOptionsList();

        // Check if option is allowed / known
        if(!$allowedOptionsList->hasAllowedOption($optionName)){
            throw new UnknownOptionException(sprintf('Option "%s" is unknown and or not supported', $optionName));
        }

        // Get the default value
        $value = $allowedOptionsList->getAllowedOption($optionName)->getDefaultValue();

        // Get user-defined value, if any is available
        if($this->hasArrayOption($optionName)){
            $value = $this->getArrayOptionValue($optionName);
        }

        return $value;
    }

    public function doesAllowOptions(){
        $definedAllowedOptions = $this->getAllowedOptionsList()->count();
        if($definedAllowedOptions > 0){
            return true;
        }
        return false;
    }

    public function doesRequireOptions(){
        $requiredOptions = count($this->getAllowedOptionsList()->getRequiredOptions());
        if($requiredOptions > 0){
            return true;
        }
        return false;
    }

    /**
     * <b>Override</b>
     *
     * Alias for getArrayOptions
     *
     * @see getArrayOptions()
     *
     * @return array
     */
    public function toArray() {
        return $this->getArrayOptions();
    }

    /**
     * <b>Override</b>
     *
     * Get the amount of options available in this list
     *
     * <b>Note</b><p>
     * This method doesn't count the amount of options that have
     * been set/specified, but rather how many options are defined
     * <p>
     *
     * @return int Amount of options available in this list
     */
    public function count() {
        return $this->getAllowedOptionsList()->count();
    }


}